**production hosted url : https://gain-impact-prod.herokuapp.com/**

**staging hosted url : https://gain-impact-chat-app.herokuapp.com/**

![](chatApp.png)

# Steps to Start the App

## Install the Public Dependencies
## Install the Server Dependencies
## Now start the server by yarn start
## Now start the react by yarn start
## And the chat application would be running successfully by now.
